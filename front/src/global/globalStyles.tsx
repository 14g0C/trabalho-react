export const global = {
    colors: {
        branco: '#ffffff',
        preto: '#000000',
        laranja: '#fca311',
        azul: '#14213d',
        cinza: '#d9d9d9',
    },
    fonts: {
        beviet: 'Be_Vietnam',
    }
}