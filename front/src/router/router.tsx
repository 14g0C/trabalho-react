import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Login from "../Pages/LogIn";
import SignUp from "../Pages/SignUp";

export default function Router() {
    const Stack = createStackNavigator();

    return(
        <>
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen
                    name = 'Login'
                    component={Login}
                    options={{
                        headerShown: false
                    }}/>

                    <Stack.Screen
                    name = 'SignUp'
                    component={SignUp}
                    options={{
                        headerShown: false
                    }}/>
                </Stack.Navigator>
            </NavigationContainer>

        </>
    );
};

