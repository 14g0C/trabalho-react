import styled from 'styled-components/native';
import { global } from '../../global/globalStyles';


//-----------------------------------------------------

export const Container = styled.View `
    flex: 1;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-top: 5%;

    background: ${global.colors.branco};
`;

//-----------------------------------------------------
export const BackSpace = styled.TouchableOpacity `
    display: flex;
    flex-direction: row;
    align-self: flex-start;
`;
export const BackButton = styled.Image `
    display: flex;
    width: 40px;
    height: 40px;
    margin-left: 20px;
`;

//-----------------------------------------------------

export const Reuseboard = styled.Image `
    width: 80%;
    height: 22%;
    border-radius: 15px;
    margin-bottom: 12%;
    margin-top: 15%
`;

//-----------------------------------------------------

export const InputView = styled.View `
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: -7%;
`;

export const InputText = styled.Text `
    color: ${global.colors.azul};
    font-size: 20px;
    margin-right: 250px;
    margin-bottom: 5px;
`;

export const DataBox = styled.View `
    width: 320px;
    height: 50px;
    color: ${global.colors.cinza};
    background-color: ${global.colors.laranja};
    border: 4px;
    border-color: ${global.colors.cinza};
    border-radius: 12px;
    margin-bottom: 50px;
`;

export const DataInput = styled.TextInput `
    flex: 1;
    font-size: 18px;
    padding: 0 10px;
    border-radius: 12px
`;

//-----------------------------------------------------

export const LoginButton = styled.TouchableOpacity `
    display: flex;
    margin-bottom: 7%;
    padding: 1% 2%;
    border-radius: 7px;
    background-color: ${global.colors.cinza};
`

export const LoginText = styled.Text `
    font-size: 30px;
    color: ${global.colors.branco};
`

//-----------------------------------------------------

export const SignupText = styled.Text `
    margin-bottom: 2%;
`;

export const SignupButton = styled.TouchableOpacity `
    display: flex;
    padding: 1% 2%;
    border-radius: 7px
    background-color: ${global.colors.azul};
`;

export const SignupSign = styled.Text `
    font-size: 18px;
    color: ${global.colors.branco};
    text-color: ${global.colors.branco};
`;