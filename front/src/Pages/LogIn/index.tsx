import React from 'react';
import { Container, DataBox, DataInput, InputText,InputView, 
Reuseboard, SignupButton, SignupText, BackButton, SignupSign,
BackSpace, LoginButton, LoginText} from './style';
import InputBox from '../../components/InputBox';
import { useNavigation } from '@react-navigation/native';



export default function Login() {
    const navigation = useNavigation();
    return(
        <Container>
            <BackSpace>
                <BackButton source={require('../../assets/Voltar.png')} />
            </BackSpace>

            <Reuseboard source={require('../../assets/Reuse_board.png')} />

            <InputBox
            datatype='Login'
            holdertxt='Insira seu login'/>

            <InputBox
            datatype='Senha'
            holdertxt='Insira sua senha'/>

            <LoginButton>
                <LoginText>Entrar</LoginText>
            </LoginButton>

            
            <SignupText>Ainda não tem cadastro ?</SignupText>

            <SignupButton onPress = {()=> navigation.navigate('SignUp')}>
                <SignupSign>Cadastre-se</SignupSign>
            </SignupButton>

        </Container>
    );
};

