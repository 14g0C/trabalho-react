import { useNavigation } from '@react-navigation/native';
import React from 'react-native';
import InputBox from '../../components/InputBox';
import ButtonSignup from '../../components/ButtonSignup';
import { BackButton, BackSpace, Container, Reuseboard } from './style';


export default function SignUp() {
    const navigation = useNavigation();

    return(
        <Container>
            <BackSpace onPress={()=>navigation.navigate('Login')}>
                <BackButton source={require('../../assets/Voltar.png')}/>
            </BackSpace>

            <Reuseboard source={require('../../assets/Reuse_board.png')}/>

            <InputBox
            datatype='Nome Completo'
            holdertxt='João da Silva Gomes'/>

            <InputBox
            datatype='Login'
            holdertxt='Insira seu e-mail'/>

            <InputBox
            datatype='Senha'
            holdertxt='Insira sua senha'/>

            <InputBox
            datatype='Endereço'
            holdertxt='Ex: Av. Rio Branco, 420'/>

            <ButtonSignup/>
        </Container>
    );
};