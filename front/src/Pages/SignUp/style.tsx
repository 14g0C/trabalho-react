import styled from 'styled-components/native';
import { global } from '../../global/globalStyles';

export const Container = styled.ScrollView `
    display: flex;
    flex: 1;
    flex-direction: column;
    padding-bottom: 90px;
    background-color: ${global.colors.branco};
`;

//-----------------------------------------------------

export const BackSpace = styled.TouchableOpacity `
    display: flex;
    align-self: flex-start;
`;
export const BackButton = styled.Image `
    display: flex;
    width: 40px;
    height: 40px;
    margin-left: 20px;
    margin-top: 15%;
`;

//-----------------------------------------------------

export const Reuseboard = styled.Image `
    display: flex;
    width: 80%;
    height: 26%;
    border-radius: 15px;
    margin-bottom: 12%;
    margin-top: 15%
    align-self: center;
`;

//-----------------------------------------------------
