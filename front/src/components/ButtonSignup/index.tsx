import React from 'react-native';
import { SignupSign, SignupButton } from './style';

export default function ButtonSignup() {
    return(
        <SignupButton>
                <SignupSign>Cadastre-se</SignupSign>
        </SignupButton>
    );
};