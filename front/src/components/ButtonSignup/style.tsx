import styled from "styled-components/native";
import { global } from "../../global/globalStyles";

export const SignupButton = styled.TouchableOpacity `
    display: flex;
    width: fit-content;
    align-self: center;
    padding: 1% 2%;
    margin-top: 8%;
    border-radius: 7px
    background-color: ${global.colors.azul};
`;

export const SignupSign = styled.Text `
    font-size: 18px;
    color: ${global.colors.branco};
    text-color: ${global.colors.branco};
`;