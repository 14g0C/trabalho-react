import React from 'react-native';
import { DataBox, DataInput, InputText, InputView } from './style';


type boxInfo = {
    datatype: string;
    holdertxt: string;
}

export default function InputBox({
    datatype,
    holdertxt
}: boxInfo) {
    return(
        <InputView>
            <InputText>{datatype}</InputText>
            <DataBox>
                <DataInput placeholder={holdertxt}/>
            </DataBox>
        </InputView>
    );
};