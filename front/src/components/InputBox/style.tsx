import styled from 'styled-components/native';
import { global } from '../../global/globalStyles';

export const InputView = styled.View `
    display: flex;
    width: fit-content;
    flex-direction: column;
    align-items: center;
    align-self: center;
    justify-content: center;
    margin-top: 7%;
    margin-bottom: -7%;
`;

export const InputText = styled.Text `
    display: flex;
    align-self: flex-start;
    color: ${global.colors.azul};
    font-size: 20px;
    margin-bottom: 5px;
`;

export const DataBox = styled.View `
    width: 320px;
    height: 50px;
    color: ${global.colors.cinza};
    background-color: ${global.colors.laranja};
    border: 4px;
    border-color: ${global.colors.cinza};
    border-radius: 12px;
    margin-bottom: 50px;
`;

export const DataInput = styled.TextInput `
    flex: 1;
    font-size: 18px;
    padding: 0 10px;
    border-radius: 12px
`;
